variable "username" {
	description = "The username for your bitbucket account"
}

variable "email" {
	description = "Email is used to login to the bitbucket account"
}

variable "password" {
	description = "The password for your bitbucket account"
}
