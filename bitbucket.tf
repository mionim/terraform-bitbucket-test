# Configure the Bitbucket Provider
provider "bitbucket" {
  username = "${var.username}"
  password = "${var.password}"
}

resource "bitbucket_repository" "terraform-bitbucket-test" {
  owner      = "${var.username}"
  name       = "terraform-bitbucket-test"
  is_private = false
  description = "Testing creating Bitbucket repository using Terraform"
}
