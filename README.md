# terraform-bitbucket-test #

This was my first try at terraform. I created a bitbucket repository using the files in this repository

### Instructions ###

Assuming `terraform` is installed, and added to your PATH, do the following.

* Rename `terraform.tfvars.example` to `terraform.tfvars`
* Update `terraform.tfvars` with appropriate values
* Run `terraform init` and then `terraform apply`